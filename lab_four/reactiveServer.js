

const { Subject }  = require("rxjs");
const { fork } = require("child_process");
const url = require("url");
const subject = new Subject();
const http = require("http");
//we use http://localhost:4000/?url=file.txt

subject.subscribe( (data) => {
   
    let queryString = url.parse(data.req.url, true).query;
    let filePath = queryString.url;
    const child_process = fork('child_process.js');
    
    if(filePath)
    child_process.send(filePath);

    child_process.on("message", function(msg){

        data.res.write(msg);
    });

    child_process.on("exit", () => {
       data.res.end()
    });

}, (error) => { console.error(error) }, () => {} );

http.createServer( (req, res) => {
    subject.next({req, res});
}).listen(4000);


