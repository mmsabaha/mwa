

const os = require("os");
const rxjs = require("rxjs");
const { from } = rxjs;

let m = 2;
let c = 2;

function checkSystem(){
    console.log("Checking your System");

    return new Promise((resolve, reject) => {
        const mem = Math.ceil(os.totalmem()/(Math.pow(1024,3)));
  
        const cpus = os.cpus().length;
      
        if(mem < m || cpus < c)
        resolve({ mem: mem, cpus: cpus });
        else 
        reject("The System is Checked successfully");

    });
}


checkSystem().then( results => {
  if(results.mem < 2)
     console.log("This app needs at least 4GB of RAM");
     return results.cpus;
}).then( results => {
       if(results < 2)
        console.log("Processor is not suppotted")
}).catch( error => {
    console.log(error)
});

//from observerable

from(checkSystem()).subscribe( x => {
    if(x.mem < m)
    console.log("This app needs at least 4GB of RAM");
    if(x.cpus < c)
    console.log("Processor is not suppotted");
    }, y => console.log(y), () => {} );






