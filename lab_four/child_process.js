

const fs = require("fs");




 process.on("message", (msg) => {

    var file  = fs.createReadStream(msg);
    
    file.setEncoding('utf8');
    file.on("data", function(chunk){
        
        if(!process.send(chunk))
        file.pause();
    
        });
    
    file.on("drain", function(){

        file.resume();
    });
    
    file.on("end", function(){

        process.exit();
        })

 
 });