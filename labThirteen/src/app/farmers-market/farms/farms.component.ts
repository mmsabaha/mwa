import { DbService } from './../db.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farms',
  templateUrl: './farms.component.html',
  styleUrls: ['./farms.component.css'],
  providers: [DbService]
})
export class FarmsComponent implements OnInit {

  private farms;

  constructor(private dbService:DbService) {

    this.farms = dbService.getData();

   }

  ngOnInit() {
  }

}
