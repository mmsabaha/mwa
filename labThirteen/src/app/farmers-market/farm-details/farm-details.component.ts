import { DbService } from './../db.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-farm-details',
  templateUrl: './farm-details.component.html',
  styleUrls: ['./farm-details.component.css']
})
export class FarmDetailsComponent implements OnDestroy {

  private id: string;
  private farmName:string;
  private products: string[];

  private idSubscription: Subscription;

  ngOnDestroy(): void {
    //  throw new Error("Method not implemented.");
    this.idSubscription.unsubscribe();
  }

  constructor(private farm_route: ActivatedRoute, private dbService: DbService) {
    this.idSubscription = farm_route.params.subscribe(
      param => {
        this.id = param['id'];
        const farm = dbService.getData().filter( d => d._id == parseInt(this.id))[0];
        this.farmName =farm.farm;
        this.products = farm.produce;
      }
    );
  }


}
