import { Injectable } from '@angular/core';

@Injectable()
export class DbService {

  getData(){

    let data = [{_id: 1, farm: "Natural Prairie", produce: ["lettuce", "tomato"]}, 
    {_id: 2, farm: "Shooting Star Oranic Farm", produce: ["cucumber", "corn", "potato"]},
    {_id: 3, farm: "Morning Sun Herb Farm", produce: ["garlic", "onion", "ginger"]},
    {_id: 4, farm: "Sonoma Broadway Farm", produce: ["Rice", "tomato", "corn", "poptato", "carrot"]},
    {_id: 5, farm: "Impossible Acres Farm", produce: ["Wheat", "tomato", "corn", "poptato", "carrot"]}];

    return data;

  }

  constructor() { }

}
