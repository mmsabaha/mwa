
import { RouterModule, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmsComponent } from './farms/farms.component';
import { FarmDetailsComponent } from './farm-details/farm-details.component';
import { FarmDetailsGuard } from './farm-details.guard';
import { DbService } from './db.service';


export const farmRoutes = [
    { path: "farms_details/:id", component: FarmDetailsComponent , canActivate: [FarmDetailsGuard] },
    { path: "farms_details", component: FarmDetailsComponent , canActivate: [FarmDetailsGuard] }
]
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(farmRoutes)
  ],
  declarations: [FarmsComponent, FarmDetailsComponent],
  providers: [FarmDetailsGuard, DbService]
})
export class FarmersMarketModule { }
