import { DbService } from './db.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FarmDetailsGuard implements CanActivate {

  constructor(private router: Router, private dbService: DbService) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let goodRoute: boolean = !!parseInt(route.params["id"]);
    let goodId = false;
    if (goodRoute) {
      if (this.dbService.getData().filter(d => d._id == parseInt(route.params["id"])).length != 0)
        goodId = true;
    }

    if (goodId) {
      return true;
    }

    this.router.navigate(["farms"]);
    alert("You didn't pass Id, or Invalid Id ");
    return false;



  }
}
