import { HomeComponent } from './home/home.component';
import { farmRoutes } from './farmers-market/farmers-market.module';
import { FarmsComponent } from './farmers-market/farms/farms.component';
import { AppComponent } from './app.component';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: "", component: HomeComponent},
  { path : "farms", component: FarmsComponent, children: farmRoutes  },
  { path : "farms_lazy", component: FarmsComponent, loadChildren : './farmers-market/farmers-market.module#FarmersMarketModule' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
