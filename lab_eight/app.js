const express = require("express");
const mongo = require("mongodb");
const Locations = require("./locations.js");
const locations = new Locations();


const app = express();
//app.use(express.urlencoded({ extended: false }));
const jsonParser =  express.json();
const col = "locations";


app.get("/", (req, res) => {

    locations.connectTodb().subscribe(data => {

        data.db.collection(col).find().toArray((err, docs) => {

            res.end(JSON.stringify(docs));

        });

    },
        err => { res.end(err.message) },
        () => { })
});

app.get("/createIndex", (req, res) => {

    locations.connectTodb().subscribe(data => {

       data.db.collection(col).createIndex({ location : '2d'});
       data.client.close();
        res.end();

    },
        err => { res.end(err.message) },
        () => { })
});

app.get("/nearestPlaces", (req, res) => {

    locations.connectTodb().subscribe(data => {

     //data.db.collection("locations").createIndex({ location : '2d'});
     data.db.collection(col).find({location:{$near: [-91.9665342, 41.017654] }}).limit(3).toArray((err, docs) => {
        if(err) throw err;
         res.end(JSON.stringify(docs));
        data.client.close();
    });

    },
        err => { res.end(err.message) },
        () => { })
});

app.get("/:id", (req, res) => {

    locations.connectTodb().subscribe(data => {
        let id = req.params.id;
        console.log(id);
        data.db.collection(col).findOne({'_id': mongo.ObjectID(id)},(err, doc) => {

            res.end(JSON.stringify(doc));

        });

    },
        err => { res.end(err.message) },
        () => { })
});


app.post("/", jsonParser,  (req, res) => {

    let newDoc = req.body;

  

    locations.connectTodb().subscribe(data => {

        data.db.collection(col).insert(newDoc, (err, docsInserted) => {
             data.client.close();
        });

    },
        err => { res.end(err.message) },
        () => { });

        res.end(JSON.stringify(req.body));
});

app.put("/", jsonParser, (req, res) => {
    
    let doc = req.body;

    locations.connectTodb().subscribe(data => {

        data.db.collection(col).update({'_id': mongo.ObjectID(req.body._id)},
         {$set : { 'name' :doc.name, 'category':doc.category, 'location':doc.location}},(err, docs) => {
            if(err) throw err;
            res.end(JSON.stringify(docs));
            data.client.close();
        });
    },
        err => { res.end(err.message) },
        () => { })
});

app.delete("/:id", (req, res) => {

    locations.connectTodb().subscribe(data => {

        data.db.collection(col).remove({'_id': mongo.ObjectID(req.params.id)},(err, docs) => {
            if(err) throw err;
            res.end(JSON.stringify(docs));
            data.client.close();
        });
    },
        err => { res.end(err.message) },
        () => { })
});





app.listen(4000);