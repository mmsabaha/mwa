class Locations {

    constructor() {

        const mongo = require("mongodb");
        const { Observable } = require("rxjs");
        this.mongoClient = mongo.MongoClient;
        this.url = "mongodb://127.0.0.1:27017/locationsdb";
        this.observable = Observable;

    }

    connectTodb() {

        return this.observable.create((observer => {

            this.mongoClient.connect(this.url, (err, client) => {

                if (!err) {
                    const db = client.db("locationsdb");
                    observer.next({ client, db });
                }
                else
                    observer.error(err);
            })

        }).bind(this));
    }


}

module.exports = Locations;


