

const express = require("express");
const app = express();
const fetch = require("node-fetch");

const { from } = require("rxjs");

app.set('x-powered-by', false);
app.enable('case sensitive routing');

app.listen(3001);

app.get("/", function(req, res){

    res.end();
});

app.get("/users", function(req, res){

    let d; 
    let observable = from(fetch("http://jsonplaceholder.typicode.com/users").then(data => data.json()));

    observable.subscribe( data => {

         res.end(JSON.stringify(data));
  
    });    
    
});
