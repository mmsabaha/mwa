


const express = require("express");
const app = express();
const fetch = require("node-fetch");

const { from } = require("rxjs");


async function respond(res){
     
    let response;

    await fetch("http://jsonplaceholder.typicode.com/users").then(data => data.json()).then(data => {response = data});

    res.send(response);

}


app.set('x-powered-by', false);
app.enable('case sensitive routing');
app.listen(3002);

app.get("/", function(req, res){

    res.end();
});

app.get("/users", function(req, res){

   respond(res);
    
});
