

const express = require("express");
const app = express();
const fetch = require("node-fetch");


app.set('x-powered-by', false);
app.enable('case sensitive routing');

app.listen(3000);

app.get("/", function(req, res){

    res.end();
});

app.get("/users", function(req, res){

    fetch("http://jsonplaceholder.typicode.com/users")
    .then( data => data.json() ).then( data => {
        res.setHeader('Content-Type', 'application/json')
        res.send(JSON.stringify(data));
    })

});

