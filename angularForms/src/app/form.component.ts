import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent  {

  user;
  myForm: FormGroup
  redirecting: boolean

  constructor(public fb: FormBuilder, public userService: UserService, private router: Router) {

    this.myForm = fb.group({
      'name': ['', Validators.required], 'email': ['', Validators.required], 'post': [' ', Validators.minLength(10)]
    })


    this.myForm.valueChanges.subscribe(x => {

      if (this.myForm.valid){
        console.log(x);
        this.redirecting = true;
        setTimeout( () => this.submitData() , 3000)
      }
       
    }
    )


  }

  getData() {

    this.userService.getUser().subscribe(user => {
      this.myForm.controls['name'].setValue(user["name"]);
      this.myForm.controls['email'].setValue(user["email"]);
    });

    this.userService.getPosts().subscribe(posts => {
      this.myForm.controls['post'].setValue(posts[0]["body"]);
    })

  }

  submitData(){
     this.router.navigate(["thank-you"]);
  }
}
