import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  constructor(public http:HttpClient) { }

  public getUser(){
    return this.http.get("https://jsonplaceholder.typicode.com/users/1");
  }

  public getPosts(){
    return this.http.get("https://jsonplaceholder.typicode.com/posts?userId=1");
  }

}
