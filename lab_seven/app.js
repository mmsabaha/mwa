

const express = require("express");
const app = express();
const Secret = require("./reveal_secret");  //module containing class Secret
const crypto = require("crypto");
const { from } = require("rxjs");

const secret = new Secret();

function decipheredMessage(secretMessage, algorithm, password) {
    const decipher = crypto.createDecipher(algorithm, password);
    let revealedMessage = decipher.update(secretMessage, 'hex', 'utf8');
    revealedMessage += decipher.final('utf8');
    return revealedMessage;
}

//using http://localhost:4000/secret

app.listen(4000);

app.get("/secret", function (req, res) {

    //insert the message for testing, assuming it is not in the database
    secret.insertMessage();

    secret.connectionToDbObservable().subscribe((data) => {

        data.db.collection("homework7").findOne({}, function (err, doc) {
            if (err) throw err;
            res.send(decipheredMessage(doc.message, 'aes256', 'asaadsaad'));
            data.client.close();
        });

    }, err => { res.end(err.message) }, () => { });

});
