

class Secret {

    constructor() {

        const mongo = require("mongodb");
        this.mongoClient = mongo.MongoClient;
        this.url = "mongodb://127.0.0.1:27017/secretdb";
        this.db;

        const { Observable } = require("rxjs");
        this.observable = Observable;


    }

    connectionToDbObservable() {
        //using Observable
        return this.observable.create((observer => {
            this.mongoClient.connect(this.url, function (err, client) {

                if (err) observer.error(err);
                else
                    observer.next({ client: client, db: client.db("secretdb") });
            });
        }).bind(this));
    }

    connectToDbPromise() {
        //using Promise
        return new Promise(((resolve, reject) => {

            this.mongoClient.connect(this.url, function (err, client) {
                if (err) reject(err);
                else
                    resolve({ client: client, db: client.db("secretdb") });
            });
        }).bind(this));
    }

    insertMessage() {
        //using Promise
        this.connectToDbPromise().then(data => {
            data.db.collection("homework7").insert({ message: "ba12e76147f0f251b3a2975f7acaf446a86be1b4e2a67a5d51d62f7bfbed5c03" });
            data.client.close();
        }).catch(err => console.error(err));

    };

}

module.exports = Secret;
