
const fs = require('fs');
var path = require('path');

const zlib = require('zlib');




function zip() {
    var gzip = zlib.createGzip();
    var readable = fs.createReadStream(path.join(__dirname, 'sample_file.md'));
    var compressed = fs.createWriteStream(path.join(__dirname, 'sample_file_compressed.md.gz'));
    readable.pipe(gzip).pipe(compressed);
}

function unzip() {

    var gunzip = zlib.createGunzip();
    var uncompressed = fs.createWriteStream(path.join(__dirname, 'sample_file_uncompress.md'));
    var anotherReadable = fs.createReadStream(path.join(__dirname, 'sample_file_compressed.md.gz'));

    anotherReadable.pipe(gunzip).pipe(uncompressed);
}

zip();
//unzip();



