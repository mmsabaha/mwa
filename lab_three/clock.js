

'use strict'
var EventEmitter = require("events")
class Clock extends EventEmitter {
    constructor() {
        super();

        setInterval(this.emit.bind(this, "tick", "woohoo"), 1000);
    }

}


var clock = new Clock();

clock.on("tick", function (data) {

    console.log(data);
});
