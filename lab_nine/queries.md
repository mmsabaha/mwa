
1.  db.zips.aggregate([{$match : {state : 'IA'}}])
2.  db.zips.aggregate([{$match : {pop : {$lt:1000}}}])
3. db.zips.aggregate([{$group : { _id: { "state": "$state", "city":"$city"}, zips : {$sum : 1}}}, {$match: {zips: {$gt: 1}}},{$project : {_id:0, state: "$_id.state", city:"$_id.city"}}, {$sort : {state:1, city:1}}])
4. db.zips.aggregate([{$group : { _id: { "state": "$state", "city":"$city"}, pop : {$sum :"$pop"}}}, {$sort : {"_id.state":1, pop: 1}}, {$group: {_id: {"state": "$_id.state"}, city : {$first : "$_id.city"}, pop: {$first : "$pop"}}}, {$project : {_id:0, state: "$_id.state", city:"$city", pop:1}}, {$sort: {state:1}}])