
class Result {

    constructor(){

        this.grades = this.grades();
    }

    grades(){
        
            return [
           { id:1 , name: "Antony", course: "MWA", grade: "95"},
           { id:2 , name: "John", course: "MWA", grade: "95"},
           { id:3 , name: "Joseph", course: "MWA", grade: "95"},
           { id:4 , name: "Alejandro Nobore", course: "MWA", grade: "95"},
           { id:5 , name: "Juliana Santamarina", course: "MWA", grade: "95"},
           { id:6, name: "Zuria Vega", course: "MWA", grade: "95"}
            ]
        
    }

    filterById(grades, id){
        
        for(let i =0; i < grades.length; i++){
          
            if(grades[i].id == id)
            return grades[i];
        }

        return null;
    }

    delete(grades, id){
        
    
        let newGrades = [];
        let counter = 0;

        for(let i =0; i < grades.length; i++){
          
            if(grades[i].id != id){
                newGrades[counter] = grades[i];
                counter++;
            }
           
        }

      
        return newGrades;
    }

    insert(grades, newGrade){

        grades.push(newGrade);

        return grades;
    }

    update(grades, newGrade){
        

        for(let i =0; i < grades.length; i++){
          
            if(grades[i].id == newGrade.id){
                
                grades[i] = newGrade;
            }
           
        }

        return grades;
    }
}


module.exports = Result;