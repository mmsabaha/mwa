var express = require('express');
var router = express.Router();
const Result = require("./../grades");


const jsonParser =  express.json();

const result = new Result();
let grades = result.grades;


router.get('/', function(req, res, next) {
  let r = JSON.stringify(grades);
  res.end(r);
});

router.get('/:id', function(req, res, next) {
    let grade = result.filterById(grades, req.params.id);

    grade = JSON.stringify(grade);
    res.end(grade);
  });

router.post('/', jsonParser, function(req, res, next) {
  
    req.assert('id', 'id is required').notEmpty();
    req.assert('name', 'name is required').notEmpty();
    req.assert('course', 'course is required').notEmpty();
    req.assert('grade', 'grade is required').notEmpty();

    let errors = req.validationErrors();

    if(errors)
     res.end(JSON.stringify(errors));

    result.insert(grades, req.body);
   
    res.end(JSON.stringify(grades));
  });


  router.put('/', jsonParser, function(req, res, next) {

    req.assert('id', 'id is required').notEmpty();
    req.assert('name', 'name is required').notEmpty();
    req.assert('course', 'course is required').notEmpty();
    req.assert('grade', 'grade is required').notEmpty();

    let errors = req.validationErrors();

    if(errors)
     res.end(JSON.stringify(errors));

    result.update(grades, req.body);
   
    res.end(JSON.stringify(grades));

  });

  router.delete('/:id', function(req, res, next) {
    
    grades = result.delete(grades, req.params.id);

    let response  = JSON.stringify(grades);
    res.end(response);

  });

module.exports = router;