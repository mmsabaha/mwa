import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  template: `<p><button (click) = "minus()">-</button>{{counterValue}}<button (click) = "plus()">+</button></p>`,
  styles: ['p { margin:50px auto; width:200px; text-align:center; font-size: 20px; border-color: green; border-style:solid } button { margin: 20px; }']
})
export class CounterComponent {

  counterValue:number;
  @Input() counter:number
  @Output() counterChange:EventEmitter<number>;

  constructor() { 
    this.counterChange = new EventEmitter();
  }

  minus(){
    if(this.counterValue > 0)
    this.counterValue--;
    this.counter = this.counterValue;
    this.counterChange.emit(this.counterValue);
  }

  plus(){

    this.counterValue++;
    this.counter = this.counterValue;
    this.counterChange.emit(this.counterValue);
  }

  ngOnChanges(){
     this.counterValue = this.counter;
  }
}
