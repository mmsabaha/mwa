import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<p>Input: <input #counterInput type='number' (input) = "changeCounter(counterInput)"/>
  <app-counter counter={{componentCounterValue}} (counterChange)="listenCounterChange($event)" ></app-counter>
  Component Counter Value is {{componentCounterValue}}</p>
 `,
  styles: ['p { margin:50px auto; width:300px; text-align:center; font-size: 20px; border-color: red; border-style:solid }',
          'input[type="number"] { width:50px;}']
})
export class AppComponent {

  componentCounterValue:number;
  @ViewChild('counterInput') myInput; 

  changeCounter(counterInput){
    this.componentCounterValue = counterInput.value;
  }

  listenCounterChange(value){
     this.myInput.nativeElement.value = value;
     this.changeCounter(this.myInput.nativeElement);
  }
 
}
