Array.prototype.isDual = function(){

    if(this.length%2 > 0 ) return 0;

    for(let i = 0; i < this.length; i += 2){

        if( i > 0){
            if(this[i] + this[i + 1] !== this[i-2] + this[i-1]){
                return 0
            }

        }

    }

    return 1;
}

console.log([1,2,3,0].isDual())