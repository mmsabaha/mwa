const tree = { 
	name : "home", 
	files : ["notes.txt","todo.txt"], 
	subFolders: [	
		{ name : "payroll", 
		  files : ["paper.pdf","funds.csv"], 
		  subFolders: [] 
		}, 
		{ name: "misc", 
		  files : ["summer1.jpg","summer2.jpg", "summer3.jpg"], 
		  subFolders: [
			{ name : "logs", 
			  files : ["logs1","logs2","logs3","logs4"], 
			  subFolders: [] 
		  }] 
	}] 
}; 

find = fileName => tree => {
  
     
     for(let i = 0; i < tree.files.length; i++){
     
     if(tree.files[i] === fileName){
       return true;
       }
     
     }
         
      if(tree.subFolders.length === 0)
        return false;
        
        for(let i = 0; i < tree.subFolders.length; i++){
         if(find(fileName)(tree.subFolders[i]))  //self call
           return true;
        }
        
        return false;
    
}

    


console.log(find("logs1")(tree)); // true 
console.log(find("randomfile")(tree)); // false