import { element } from 'protractor';
import { Directive, Renderer2, ElementRef, Input, HostListener, HostBinding, Output, EventEmitter } from '@angular/core';
import { callLifecycleHooksChildrenFirst } from '@angular/core/src/view/provider';

@Directive({
  selector: '[appMycolor]'
})
export class MycolorDirective {

  @Input() appMycolor
  @Output() theColor:EventEmitter<String> = new EventEmitter();

  myColors = ["red", "green", "blue", "yellow", "grey", "orange"];
  counter:number = 0;
  
  @HostBinding('style.backgroundColor') myColor;

  @HostListener("click") click(){

  if(this.counter == this.myColors.length)
    this.counter = 0;
    this.myColor = this.myColors[this.counter];
    this.theColor.emit(this.myColors[this.counter]);
    this.counter++;
  }

  ngOnChanges(){

    if(this.appMycolor <= this.myColors.length)
    this.counter = this.appMycolor;
  }

}
