import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-my-list',
  template: `
    <h1>
      my-list works!
    </h1>
    <ul *ngFor = "let str of myArray; let index = index">
     <li [appUpper] [appMyvisibility] = true [appMycolor] = "index" (theColor) = "getColor($event)"  > {{str}}  </li>
    </ul>
  `,
  styles: [],

})
export class MyListComponent implements OnInit {

  @Input() myArray
  @Output() colourOut:EventEmitter<string> = new EventEmitter();

  color:string = "red";
  constructor() { }
  getColor(color){
    this.color = color;
    this.colourOut.emit(color);
  }
  ngOnInit() {
  }

  ngChanges(){
    console.log("aa");
  }

}
