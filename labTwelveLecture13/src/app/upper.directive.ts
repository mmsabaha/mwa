import { Directive, ElementRef, Renderer2, Input, HostBinding } from '@angular/core';

@Directive({
  selector: '[appUpper]'
})
export class UpperDirective {
  @Input() appUpper;

  constructor(e: ElementRef, r: Renderer2) {

    r.setStyle(e.nativeElement, "text-transform", "uppercase");

  }


}
