import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<div class='container'>
             <p>{{childColor ? "Last color changed to " + childColor : "Click an element to see the color and text, click more than once in the same element to get different colors"}}</p>
             <app-my-list [myArray] = "parentArray" (colourOut) = "getColor($event)"></app-my-list>
             </div>`,
  styles: ['ul { margin: 10px; padding:10px;} .container { margin-left: 100px; } '],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'app';
  childColor;
  parentArray = ["Javascript", "Nodejs", "Express", "Mongodb", "Angular"];

  getColor(color){
this.childColor = color;
  }
}
