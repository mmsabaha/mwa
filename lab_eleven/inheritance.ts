
class BaseObject{

    width:number = 0;
    length: number = 0;
}


class Rectangle extends BaseObject {

    constructor(){
        super();
        this.width = 5;
        this.length = 2
    }

    public calculateSize():number{

        return this.width * this.length;
    }
}

console.log((new Rectangle()).calculateSize())