
class Person {

    private _firstName: string;

    get firstName():string{

        return this._firstName;
    }

    set firstName(value:string){

        this._firstName = value;
    }
}

let myPerson:Person = new Person();
myPerson.firstName = "Asaad";
console.log(myPerson.firstName);